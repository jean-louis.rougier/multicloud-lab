# Multicluster yelb deployment using NSM

The goal of this lab is to deploy the [yelb](https://github.com/mreferre/yelb) app on three different Kubernetes clusters using [Network Service Mesh](https://networkservicemesh.io/) (NSM), and more specifically its interdomain vL3 service.

This lab is based on the [interdomain vL3 deployment example](https://github.com/networkservicemesh/deployments-k8s/tree/5cde16085cbecccac290a7b1dc07103890c78b47/examples/multicluster/usecases/floating_vl3-basic) of NSM.

To start this lab, you need a Linux environment with:

- [Docker](https://docs.docker.com/engine/install/)
- [kind](https://kind.sigs.k8s.io/docs/user/quick-start/)
- The [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/) tool

In order for this lab to work properly, some inotify limits must be raised. To do so, copy [10-inotify.conf](./10-inotify.conf) to `/etc/sysctl.d/`.
Then, either restart the machine or run `sysctl --system`.

*This lab was tested on a Debian 11 VM with 16 GB of memory.*

*Network Service Mesh uses [VPP](https://s3-docs.fd.io/vpp/23.02/), which requires a CPU supporting the SSE4.2 instruction set extension to work. This can be verified by running the `lscpu` command and checking if the CPU flags contain `sse4_2`.*

## Introduction

### Yelb

[Yelb](https://github.com/mreferre/yelb) is a sample application composed of four pods:

- `yelb-ui` - A frontend containing an [Angular](https://angular.io/) application and an [Nginx](https://www.nginx.com/) reverse proxy
- `yelb-appserver` - A backend containing a [Ruby](https://www.ruby-lang.org/en/) application
- `yelb-db` - A [PostgreSQL](https://www.postgresql.org/) database used by the backend
- `yelb-cache` - A [Redis](https://redis.com/fr/solutions/cas-dutilisation/cache/) cache used by the backend

<div align="center">

![Architecture of the yelb application](./img/yelb-architecture.png)

*Architecture of the yelb application*

</div>

Our goal is to deploy this application on two different Kubernetes clusters, by splitting the app in two parts:

- The frontend and the PostgresSQL database will be in the first cluster.
- The *appserver* backend and the redis cache will be in the second cluster. It makes sense to keep the cache close to the appserver for performance reasons

<div align="center">

![Architecture of the split yelb application](./img/yelb-split.png)

*Architecture of the split yelb application*

</div>

### Network Service Mesh

In order to establish communication between the pods in our two Kubernetes clusters, we will use [Network Service Mesh](https://networkservicemesh.io/) (NSM). NSM is a generic, non Kubernetes-specific, service mesh that can connect *workloads* to *Network Services*. Generally speaking, an Network Service is a service that handles IP packets or Ethernet frames to provide a feature such as encryption, firewalling, or more advanced features such as tunneling. If necessary, Network Services can even be chained to provide an overall more complex service.

This connection is done by associating workloads with a *Network Service Client*, or *NSC* (in Kubernetes, an NSC is a sidecar container), and *Network Services* with *Network Service Endpoints*, or *NSEs* (in Kubernetes, NSEs are pods that make the Network Service work).

<div align="center">

![Connecting workloads to network services](./img/netsvc.svg)

*Connecting workloads to network services*

</div>

When an NSC needs to be connected to an NSE (in Kubernetes, this is done using an [annotation](https://networkservicemesh.io/docs/concepts/k8s/)), a *vWire* (virtual wire) is established between the two. In practice, an additional interface is added in the NSC's network namespace (ie: pod in Kubernetes) in such a way that every packet sent to that interface necessarily goes through the NSE to be handled by the Network Service.

A power of NSM is that workloads and Network Services don't need to be located in the same place. In NSM terms, they don't need to be in the same *Runtime Domain*. In [the NSM documentation](https://networkservicemesh.io/docs/concepts/enterprise_users/), a Runtime Domain is defined as a "system on which workloads are run". It can therefore be a Kubernetes cluster, a VM, a bare metal hypervisor, etc ... Therefore, it is entirely possible to have a workload located in a Kubernetes cluster connect to an Network Service located in another Kubernetes cluster (or any other Runtime Domain) - effectively achieving inter-cluster communication.

Here, we want to connect workloads running in separate Kubernetes clusters. Therefore, we will use the [vL3 Network Service](https://github.com/networkservicemesh/deployments-k8s/tree/main/examples/multicluster/usecases/floating_vl3-basic).

### The vL3 Network Service

The vL3 network service enables us to create a virtual IP network connecting several pods. this Network Service is a bit special since it isn't located in a specific cluster: it is used to *connect* workloads located in *different* clusters. However, due to the way NSM works, it still has to be registered somewhere. Here, we're using a third cluster to host the Network Service, known as the *floating domain*. In practice, in addition to the Network Service being physically in the cluster's NSM registry, the cluster will also host the IPAM service used to provide IPs to NSCs that ask to join the vL3 network:

<div align="center">

![Structure of the interdomain vL3 service](./img/vl3.png)

*Structure of the interdomain vL3 service*

</div>

Inter-domain communication is effectively achieved thanks to *NSE-NSE vWires*. Technically speaking, a vL3 NSE is considered as an NSC for the other vL3 NSEs it connects to, so the regular vWires system can be used here. However, these vWires are somewhat special since they connect an NSC to an NSE that is not located in the same *Runtime Domain*. Therefore, they are called [*interdomain vWires*](https://github.com/networkservicemesh/deployments-k8s/tree/main/examples/multicluster), and are established using the floating domain's registry.

### Final structure of this lab

When everything is successfully deployed, the yelb pods will successfully be able to communicate with each other over the vL3 network. They will connect to their local vL3 NSE via an additional interface initialized by an attached NSC sidecar:

<div align="center">

![Final structure of this lab](./img/structure.svg)

*Final structure of this lab*

</div>

## Creation of the lab's environment

This lab requires three Kubernetes clusters that can provision `LoadBalancer` services.

To create such clusters, the provided `create-env.sh` script can be used to create three `kind` clusters and install `MetalLB` inside them.

**The variables KUBECONFG1, KUBECONFG2 and KUBECONFG3 are used throughout the lab**. In order for them to be available, please execute the command bellow:

    source ./activate

## Inter-cluster DNS setup

When used in a multicluster fashion, part of NSM's control plane need to be exposed publicly using Kubernetes `Service`s. To facilitate service discovery, DNS is used: therefore, for instance, a service named `registry` exposed on `cluster2` can be contacted using the `registry.my.cluster2` domain name.

To provide such a feature, Kubernetes' [CoreDNS](https://coredns.io/) system is used. First, this service has to be exposed:

    kubectl --kubeconfig=$KUBECONFIG1 expose service kube-dns -n kube-system --port=53 --target-port=53 --protocol=TCP --name=exposed-kube-dns --type=LoadBalancer
    kubectl --kubeconfig=$KUBECONFIG2 expose service kube-dns -n kube-system --port=53 --target-port=53 --protocol=TCP --name=exposed-kube-dns --type=LoadBalancer
    kubectl --kubeconfig=$KUBECONFIG3 expose service kube-dns -n kube-system --port=53 --target-port=53 --protocol=TCP --name=exposed-kube-dns --type=LoadBalancer

The clusters' `LoadBalancer` provisioner should now give them public IPs. These IPs will need to be shared between the clusters. Fortunately, once DNS service discovery is set, we won't have to do it anymore!

We therefore need to retrieve the IPs given to the newly exposed services:

    ip1=$(kubectl --kubeconfig=$KUBECONFIG1 get services exposed-kube-dns -n kube-system -o go-template='{{index (index (index (index .status "loadBalancer") "ingress") 0) "ip"}}')
    ip2=$(kubectl --kubeconfig=$KUBECONFIG2 get services exposed-kube-dns -n kube-system -o go-template='{{index (index (index (index .status "loadBalancer") "ingress") 0) "ip"}}')
    ip3=$(kubectl --kubeconfig=$KUBECONFIG3 get services exposed-kube-dns -n kube-system -o go-template='{{index (index (index (index .status "loadBalancer") "ingress") 0) "ip"}}')

Then, we need to configure the instances of CoreDNS to:

- Allow the local exposed services to be resolved via the `my.clusterN` domain. This is done by using the `k8s_external` plugin.
- Forward the request regarding the other `my.clusterN` domains to the right cluster. This is done by hardcoding the IP addresses we got from the last commands.

For cluster1:

```yaml
cat <<EOF | kubectl apply --kubeconfig=$KUBECONFIG1 -f -
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: coredns
  namespace: kube-system
data:
  Corefile: |
    .:53 {
        errors
        health {
            lameduck 5s
        }
        ready
        kubernetes cluster.local in-addr.arpa ip6.arpa {
            pods insecure
            fallthrough in-addr.arpa ip6.arpa
            ttl 30
        }
        k8s_external my.cluster1
        prometheus :9153
        forward . /etc/resolv.conf {
            max_concurrent 1000
        }
        loop
        reload 5s
    }
    my.cluster2:53 {
      forward . ${ip2}:53 {
        force_tcp
      }
    }
    my.cluster3:53 {
      forward . ${ip3}:53 {
        force_tcp
      }
    }
EOF
```

For cluster2:

```yaml
cat <<EOF | kubectl apply --kubeconfig=$KUBECONFIG2 -f -
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: coredns
  namespace: kube-system
data:
  Corefile: |
    .:53 {
        errors
        health {
            lameduck 5s
        }
        ready
        kubernetes cluster.local in-addr.arpa ip6.arpa {
            pods insecure
            fallthrough in-addr.arpa ip6.arpa
            ttl 30
        }
        k8s_external my.cluster2
        prometheus :9153
        forward . /etc/resolv.conf {
            max_concurrent 1000
        }
        loop
        reload 5s
    }
    my.cluster1:53 {
      forward . ${ip1}:53 {
        force_tcp
      }
    }
    my.cluster3:53 {
      forward . ${ip3}:53 {
        force_tcp
      }
    }
EOF
```

For cluster3:

```yaml
cat <<EOF | kubectl apply --kubeconfig=$KUBECONFIG3 -f -
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: coredns
  namespace: kube-system
data:
  Corefile: |
    .:53 {
        errors
        health {
            lameduck 5s
        }
        ready
        kubernetes cluster.local in-addr.arpa ip6.arpa {
            pods insecure
            fallthrough in-addr.arpa ip6.arpa
            ttl 30
        }
        k8s_external my.cluster3
        prometheus :9153
        forward . /etc/resolv.conf {
            max_concurrent 1000
        }
        loop
        reload 5s
    }
    my.cluster1:53 {
      forward . ${ip1}:53 {
        force_tcp
      }
    }
    my.cluster2:53 {
      forward . ${ip2}:53 {
        force_tcp
      }
    }
EOF
```

## Spire setup

Now that services exposed by one cluster can easily be discovered by the others, we can setup the first service required by NSM: [Spire](https://spiffe.io/docs/latest/spiffe-about/overview/)

Spire’s goal is to associate certificates to workloads that can be used for identification and trust purposes. It uses the [SPIFFE](https://spiffe.io/docs/latest/spiffe-about/overview/) protocol. The certificates it creates are signed by a CA, that needs to be trusted by the other clusters: this is ensured by exchanging  *[trust bundles](https://spiffe.io/docs/latest/spiffe/concepts/#trust-bundle)*.

To setup Spire, we first need to install it on all three clusters:

    kubectl --kubeconfig=$KUBECONFIG1 apply -k https://github.com/networkservicemesh/deployments-k8s/examples/spire/cluster1?ref=859eaf1abd3e95b4cf7436562370e03c07d0fa68
    kubectl --kubeconfig=$KUBECONFIG2 apply -k https://github.com/networkservicemesh/deployments-k8s/examples/spire/cluster2?ref=859eaf1abd3e95b4cf7436562370e03c07d0fa68
    kubectl --kubeconfig=$KUBECONFIG3 apply -k https://github.com/networkservicemesh/deployments-k8s/examples/spire/cluster3?ref=859eaf1abd3e95b4cf7436562370e03c07d0fa68

Then, we need to wait for the Spire pods to be properly initialized:

    kubectl --kubeconfig=$KUBECONFIG1 wait -n spire --timeout=1m --for=condition=ready pod -l app=spire-server
    kubectl --kubeconfig=$KUBECONFIG1 wait -n spire --timeout=1m --for=condition=ready pod -l app=spire-agent
    kubectl --kubeconfig=$KUBECONFIG2 wait -n spire --timeout=1m --for=condition=ready pod -l app=spire-server
    kubectl --kubeconfig=$KUBECONFIG2 wait -n spire --timeout=1m --for=condition=ready pod -l app=spire-agent
    kubectl --kubeconfig=$KUBECONFIG3 wait -n spire --timeout=1m --for=condition=ready pod -l app=spire-server
    kubectl --kubeconfig=$KUBECONFIG3 wait -n spire --timeout=1m --for=condition=ready pod -l app=spire-agent

Now that Spire is installed on all three clusters, the trust bundles need to be exchanged.

We first need to get them using the following commands:

    bundle1=$(kubectl --kubeconfig=$KUBECONFIG1 exec spire-server-0 -n spire -- bin/spire-server bundle show -format spiffe)
    bundle2=$(kubectl --kubeconfig=$KUBECONFIG2 exec spire-server-0 -n spire -- bin/spire-server bundle show -format spiffe)
    bundle3=$(kubectl --kubeconfig=$KUBECONFIG3 exec spire-server-0 -n spire -- bin/spire-server bundle show -format spiffe)

Then, we have to share them among the clusters:

    echo $bundle2 | kubectl --kubeconfig=$KUBECONFIG1 exec -i spire-server-0 -n spire -- bin/spire-server bundle set -format spiffe -id "spiffe://nsm.cluster2"
    echo $bundle3 | kubectl --kubeconfig=$KUBECONFIG1 exec -i spire-server-0 -n spire -- bin/spire-server bundle set -format spiffe -id "spiffe://nsm.cluster3"

    echo $bundle1 | kubectl --kubeconfig=$KUBECONFIG2 exec -i spire-server-0 -n spire -- bin/spire-server bundle set -format spiffe -id "spiffe://nsm.cluster1"
    echo $bundle3 | kubectl --kubeconfig=$KUBECONFIG2 exec -i spire-server-0 -n spire -- bin/spire-server bundle set -format spiffe -id "spiffe://nsm.cluster3"

    echo $bundle1 | kubectl --kubeconfig=$KUBECONFIG3 exec -i spire-server-0 -n spire -- bin/spire-server bundle set -format spiffe -id "spiffe://nsm.cluster1"
    echo $bundle2 | kubectl --kubeconfig=$KUBECONFIG3 exec -i spire-server-0 -n spire -- bin/spire-server bundle set -format spiffe -id "spiffe://nsm.cluster2"

## NSM setup

Now that Spire is installed and operational, we can begin to setup NSM.

The first two clusters will have to host a local NSM control plane (ex: a registry, a VPP forwarder, ...), but as the third cluster will only be used as a *floating domain*, it only needs to host a NSM registry for now.

First, the `ClusterProperty` CRD has to be defined on the first two clusters:

    kubectl --kubeconfig=$KUBECONFIG1 apply -k https://github.com/kubernetes-sigs/about-api/clusterproperty/config/crd?ref=f9b81063dfa88c032a47789c7cd3357ba0c17f7d
    kubectl --kubeconfig=$KUBECONFIG2 apply -k https://github.com/kubernetes-sigs/about-api/clusterproperty/config/crd?ref=f9b81063dfa88c032a47789c7cd3357ba0c17f7d

Then, the different NSM components mentioned above can be deployed:

    kubectl --kubeconfig=$KUBECONFIG1 apply -k https://github.com/networkservicemesh/deployments-k8s/examples/multicluster/clusters-configuration/cluster1?ref=5cde16085cbecccac290a7b1dc07103890c78b47
    kubectl --kubeconfig=$KUBECONFIG2 apply -k https://github.com/networkservicemesh/deployments-k8s/examples/multicluster/clusters-configuration/cluster2?ref=5cde16085cbecccac290a7b1dc07103890c78b47
    kubectl --kubeconfig=$KUBECONFIG3 apply -k https://github.com/networkservicemesh/deployments-k8s/examples/multicluster/clusters-configuration/cluster3?ref=5cde16085cbecccac290a7b1dc07103890c78b47

Before we can continue, we have to wait for the `admission-webhook-k8s` app to be ready on the first two clusters:

    WH=$(kubectl --kubeconfig=$KUBECONFIG1 get pods -l app=admission-webhook-k8s -n nsm-system --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
    kubectl --kubeconfig=$KUBECONFIG1 wait --for=condition=ready --timeout=1m pod ${WH} -n nsm-system
    WH=$(kubectl --kubeconfig=$KUBECONFIG2 get pods -l app=admission-webhook-k8s -n nsm-system --template '{{range .items}}{{.metadata.name}}{{"\n"}}{{end}}')
    kubectl --kubeconfig=$KUBECONFIG2 wait --for=condition=ready --timeout=1m pod ${WH} -n nsm-system

## vL3 setup

Now that NSM is set-up on the first two clusters, and that the third cluster can host inter-domain Network Services, we can finally deploy the vL3 service:

    kubectl --kubeconfig=$KUBECONFIG1 apply -k ./vl3/cluster1
    kubectl --kubeconfig=$KUBECONFIG2 apply -k ./vl3/cluster2
    kubectl --kubeconfig=$KUBECONFIG3 apply -k ./vl3/cluster3

## Deploy the yelb app

With the vL3 service being deployed, we can now deploy the `yelb` app on the first two clusters. As explained earlier, it is composed of four pods:

- `yelb-ui` - A frontend containing an [Angular](https://angular.io/) application and an [Nginx](https://www.nginx.com/) reverse proxy
- `yelb-appserver` - A backend containing a [Ruby](https://www.ruby-lang.org/en/) application
- `yelb-db` - A [PostgreSQL](https://www.postgresql.org/) database used by the backend
- `yelb-cache` - A [Redis](https://redis.com/fr/solutions/cas-dutilisation/cache/) cache used by the backend

We chose to deploy `yelb-ui` and `yelb-db` on the first cluster, and `yelb-appserver` and `yelb-cache` on the second cluster. Since `yelb-cache` only need to communicate with its local `yelb-appserver`, it doesn't need to be connected to the vL3 network.

To deploy yelb in this way, run the following commands:

    kubectl --kubeconfig=$KUBECONFIG1 apply -f ./yelb/cluster1.yaml
    kubectl --kubeconfig=$KUBECONFIG2 apply -f ./yelb/cluster2.yaml

Note the `yelb-ui` pod may crash several times before starting successfully. Indeed, to start properly, the pod needs to be able to perform a DNS resolution for the `yelb-appserver` name, which is only possible *after* the `yelb-appserver` pod started and successfully joined the vL3 network.

Wait for all the pods to be ready:

    kubectl --kubeconfig=$KUBECONFIG1 wait --timeout=1m --for=condition=ready pod -l app=yelb-db
    kubectl --kubeconfig=$KUBECONFIG2 wait --timeout=1m --for=condition=ready pod -l app=yelb-appserver
    kubectl --kubeconfig=$KUBECONFIG2 wait --timeout=1m --for=condition=ready pod -l app=redis-server
    kubectl --kubeconfig=$KUBECONFIG1 wait --timeout=1m --for=condition=ready pod -l app=yelb-ui

When all the pods are ready, the yelb app should work!

## Speeding up the app

The app works, but *very slowly*. As explained in [this issue](https://github.com/networkservicemesh/sdk/issues/1414), the slowdown is caused by unoptimized DNS queries.

To speedup the app, DNS can be bypassed by hardcoding some IPs in the `/etc/hosts` file of specific pods:

- The `yelb-ui` pod needs to know the address of `yelb-appserver`
- The `yelb-appserver` pod needs to know the address of `yelb-db`

First, get the addresses of `yelb-appserver` and `yelb-db`:

    yelb_db_pod=$(kubectl --kubeconfig=$KUBECONFIG1 get pod -l app=yelb-db -o name)
    kubectl --kubeconfig=$KUBECONFIG1 exec $yelb_db_pod -- bash -c "apt update; apt install -y iproute2"
    yelb_db_ip=$(kubectl --kubeconfig=$KUBECONFIG1 exec $yelb_db_pod -- ip a s nsm-1 | egrep -o 'inet [0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | cut -d' ' -f2)

    yelb_appserver_pod=$(kubectl --kubeconfig=$KUBECONFIG2 get pod -l app=yelb-appserver -o name)
    kubectl --kubeconfig=$KUBECONFIG2 exec $yelb_appserver_pod -- bash -c "apt update; apt install -y iproute2"
    yelb_appserver_ip=$(kubectl --kubeconfig=$KUBECONFIG2 exec $yelb_appserver_pod -- ip a s nsm-1 | egrep -o 'inet [0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}' | cut -d' ' -f2)


Then add entries in the `/etc/hosts` file for `yelb-ui` and `yelb-appserver`:

    yelb_ui_pod=$(kubectl --kubeconfig=$KUBECONFIG1 get pod -l app=yelb-ui -o name)
    kubectl --kubeconfig=$KUBECONFIG1 exec $yelb_ui_pod -- bash -c "echo $yelb_appserver_ip yelb-appserver >> /etc/hosts"

    yelb_appserver_pod=$(kubectl --kubeconfig=$KUBECONFIG2 get pod -l app=yelb-appserver -o name)
    kubectl --kubeconfig=$KUBECONFIG2 exec $yelb_appserver_pod -- bash -c "echo $yelb_db_ip yelb-db >> /etc/hosts"

The application should now be much faster.

## Related material

- [The report associated with this project](https://notes.rezel.net/QPLzvq5kSpuHLjGMHSAVVA?view)
- [The presentation associated with the report](https://docs.google.com/presentation/d/16Iv-fvUUexyrVJd9jg8TffSY69HDPv0wCnAu5Ahx5-Q) 

## Thanks

Thanks to:

- Jean-Louis ROUGIER
- Alain FIOCCO
- Patrice NIVAGGIOLI
- Jerome TOLLET
- Ed WARNICKE
- Denis TINGAIKIN
- Artem GLAZYCHEV

For helping to bring this project to fruition.
