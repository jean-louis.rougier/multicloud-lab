#!/usr/bin/env bash

# This script is used to create the environment used in this lab
# Based on a script created by Patrice Nivaggioli

CLUSTER_TYPE=${CLUSTER_TYPE:kind}
HOSTIP_MATCH=${HOSTIP_MATCH:-10.87}
GLOBAL_METALLB_PRFX=${GLOBAL_METALLB_PRFX:-250}

CLUSTER_NAMEPRFX=${CLUSTER_NAMEPRFX:-demo}

NUM_CLUSTERS=${NUM_CLUSTERS:-3}

KIND_IMAGE=${KIND_IMAGE:-kindest/node:v1.25.3@sha256:f52781bc0d7a19fb6c405c2af83abfeb311f130707a0e219175677e366cc45d1}

#if [[ -z ${K8S_API_ADDR} ]]; then
#    K8S_API_ADDR=$(hostname -i)
#fi

# we use a static additional loopback address. this ensures that we can use the same IP independantly of the instance
# and will allow to have the cluster pre-provisioned on any instance. Pleas uncomment the previous lines if this is not desired
K8S_API_ADDR="127.0.0.1"
K8S_API_PORT=6443


# create_cluster
# - creates a kind cluster
#
# NOTE: Since SMM multicluster peers need to be able to reach each other's api-server we need to use
# a reachable api-server address in the cluster creation so the kubeconfig server field is usable from
# within the clusters.
#
function create_cluster {
    local name=$1; shift
    if [[ $# > 1 ]]; then
        local apiaddr=$1; shift
        local portoffset=$1; shift
    fi

    apiport=$((${K8S_API_PORT} + ${portoffset}))

    cat <<EOF > kind_config.yaml
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
networking:
  # WARNING: It is _strongly_ recommended that you keep this the default
  # (127.0.0.1) for security reasons. However it is possible to change this.
  apiServerAddress: "${apiaddr}"
  # By default the API server listens on a random open port.
  # You may choose a specific port but probably don't need to in most cases.
  # Using a random port makes it easier to spin up multiple clusters.
  apiServerPort: ${apiport}
nodes:
- role: control-plane
  image: ${KIND_IMAGE}
- role: worker
  image: ${KIND_IMAGE}
- role: worker
  image: ${KIND_IMAGE}
EOF
    kind create cluster --name ${name} --config kind_config.yaml
    kind get kubeconfig --name ${name} > ~/.kube/${name}.kconf
}

for (( cur_cluster=0; cur_cluster < $NUM_CLUSTERS; cur_cluster++))
do
    # peers offset starts at 2
    cluster_name=${CLUSTER_NAMEPRFX}$((${cur_cluster} + 1))
    create_cluster $cluster_name ${K8S_API_ADDR} ${cur_cluster}
done

#labstart
#curl http://master.demos.eticloud.io:8000/labnotif/start

# Find the node's first 2 bytes for use to create metallb IP ranges (assumes k3d docker networks are using /16 cidr):
kconf=~/.kube/${CLUSTER_NAMEPRFX}1.kconf
nodeAddrPrfx=$(kubectl get nodes --kubeconfig ${kconf} -o jsonpath="{.items[0].status.addresses[?(@.type=='InternalIP')].address}" | cut -d '.' -f 1,2)


# Install metallb in all the clusters (use the later 3 bytes of the docker bridge prefix)

for (( cur_cluster=0; cur_cluster < $NUM_CLUSTERS; cur_cluster++))
do
    cluster_name=${CLUSTER_NAMEPRFX}$((${cur_cluster} + 1))

    kconf=~/.kube/${cluster_name}.kconf
    kubectl apply -f https://raw.githubusercontent.com/metallb/metallb/v0.13.7/config/manifests/metallb-native.yaml --kubeconfig ${kconf}

    # Wait for the MetalLB controller to be ready
    kubectl --kubeconfig ${kconf} wait -n metallb-system --timeout=1m --for=condition=ready pod -l component=controller

    cat <<EOF | kubectl apply --kubeconfig ${kconf} -f -
---
apiVersion: metallb.io/v1beta1
kind: IPAddressPool
metadata:
  namespace: metallb-system
  name: metallb-ippool
spec:
  addresses:
  - ${nodeAddrPrfx}.${GLOBAL_METALLB_PRFX}.1-${nodeAddrPrfx}.${GLOBAL_METALLB_PRFX}.250
  autoAssign: true
  avoidBuggyIPs: false
---
apiVersion: metallb.io/v1beta1
kind: L2Advertisement
metadata:
  name: metallb-l2-mode
  namespace: metallb-system
EOF
    GLOBAL_METALLB_PRFX=$((${GLOBAL_METALLB_PRFX} + 1))
done
